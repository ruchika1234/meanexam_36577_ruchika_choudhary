const express = require('express')
const utils = require('../utils.js')
const db = require('../db')

const router = express.Router()
//-----------------------------------------------------------
// GET
//-----------------------------------------------------------


router.get('/', (request, response) => {
    const statement = `select empName, address, birth_date, gendor, mobile, profilePhoto from employee where role = 'employee '`
    db.query(statement, (error, employees) => {
        if (error) {
            response.send({ status: 'error', error: error })
        } else {
            if (employees.length == 0) {
                response.send({ status: 'error', error: 'Employee not found' })
            } else {
                const employee = employees[0]
                response.send(utils.createResult(error, employee))
            }
        }
    })

})
//-----------------------------------------------------------



//-----------------------------------------------------------
// POST
//-----------------------------------------------------------


router.post('/insert', (request, response) => {
    const { empName, address, birth_date, gendor, email, mobile, password, role, profilePhoto } = request.body

    const statment = `insert into employee (empName, address, birth_date, gendor, email, role. profilePhoto) values (
      '${empName}', '${address}', '${birth_date}', '${gendor}', '${email}', '${mobile}', '${role}','${profilePhoto}',  
  )`

    db.query(statment, (error, data) => {
        response.send(utils.createResult(error, data))
    })

})

//-----------------------------------------------------------




//-----------------------------------------------------------
// PUT
//-----------------------------------------------------------


router.put('/', (request, response) => {

})
//-----------------------------------------------------------




//-----------------------------------------------------------
// DELETE
//-----------------------------------------------------------

router.delete('/:id', (request, response) => {
    const {id} = request.params

    const statement = `delete from employee where id = ${id}`

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

//-----------------------------------------------------------


module.exports = router