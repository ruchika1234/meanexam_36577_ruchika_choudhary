const express = require('express')
const utils = require('../utils.js')
const db = require('../db')

const router = express.Router()
//-----------------------------------------------------------
// GET
//-----------------------------------------------------------


router.get('/profile', (request, response) => {
    const statement = `select * from employee`
    db.query(statement, (error, admins) => {
        if (error) {
            response.send({ status: 'error', error: error })
        } else {
            if (admins.length == 0) {
                response.send({ status: 'error', error: 'admin not found' })
            } else {
                const admin = admins[0]
                response.send(utils.createResult(error, admin))
            }
        }
    })

})
//-----------------------------------------------------------



//-----------------------------------------------------------
// POST
//-----------------------------------------------------------

router.post('/register', (request, response) => {
    const { empName, address, birth_date, gendor, email, mobile, password, role, profilePhoto } = request.body

    const statment = `insert into employee (empName, address, birth_date, gendor, email, password, role. profilePhoto) values (
      '${empName}', '${address}', '${birth_date}', '${gendor}', '${email}', '${mobile}', '${password}', '${role}','${profilePhoto}',  
  )`

    db.query(statment, (error, data) => {
        response.send(utils.createResult(error, data))
    })

})


router.post('/login', (request, response) => {
    const { email, password} = request.body

    const statement = `select empId, empName, address from employee where email = '${email}' and password = '${password}' and role = 'admin'`

    db.query(statement, (error, admins) => {
        if (error) {
            response.send({ status: 'error', error: error })
        } else {
            if (admins.length == 0) {
                response.send({ status: 'error', error: 'admin not found' })
            } else {
                const admin = admins[0]
                response.send(utils.createResult(error, {
                    empName: admin['empName'],
                    address: admin['address']

                }))
            }
        }
    })
})
//-----------------------------------------------------------




//-----------------------------------------------------------
// PUT
//-----------------------------------------------------------


router.put('/', (request, response) => {

})
//-----------------------------------------------------------




//-----------------------------------------------------------
// DELETE
//-----------------------------------------------------------

router.delete('/:id', (request, response) => {
    const {id} = request.params

    const statement = `delete from employee where id = ${id}`

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

//-----------------------------------------------------------


module.exports = router