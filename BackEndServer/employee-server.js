const express = require('express')
const bodyParser = require('body-parser')
const { request, response } = require('express')
const cors = require('cors')


//morgan: for logging:
const morgan = require('morgan')


//routers



const app = express()
app.use(cors("*"))

app.use(bodyParser.json())
app.use(morgan('combined'))




//requires to send static images in the directory named image    
app.use(express.static('images/'))


// add the routes





// default route
app.get('/', (request, response) => {
    response.send('welcome to Employee application')
})

app.listen(4000, '0.0.0.0', () => {
    console.log('server started on port 3000')
})

