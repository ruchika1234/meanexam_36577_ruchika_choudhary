const express = require('express')
const bodyParser = require('body-parser')
const { request, response } = require('express')
const cors = require('cors')


//morgan: for logging:
const morgan = require('morgan')


//routers
const adminRouter = require('./admin-routes/admin')
const employeeRouter = require('./admin-routes/employee')

const app = express()
app.use(cors("*"))

app.use(bodyParser.json())
app.use(morgan('combined'))




//requires to send static images in the directory named image    
app.use(express.static('images/'))


// add the routes
app.use('/admin', adminRouter)
app.use('/employee', employeeRouter)



// default route
app.get('/', (request, response) => {
    response.send('welcome to Admin application')
})

app.listen(3000, '0.0.0.0', () => {
    console.log('server started on port 3000')
})

