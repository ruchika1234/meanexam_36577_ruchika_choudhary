import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from '../login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  email = ''
  password = ''

  constructor(
    private router: Router,
    private loginService: LoginService
  ) { }

  ngOnInit(): void {
  }

  onLogin(){
   if(this.email.length == 0){
     alert("please fill email")
   } else if(this.password.length == 0){
     alert("please fill password")
   } else{
    this.loginService.login(this.email, this.password)
    .subscribe(response => {
      if(response['status'] == 'success'){
        const data = response['data']
        console.log(data);
        alert("login successfully")
        
      } else{
        alert("invalid email and password")
        console.log(response['error']);
        
      }
    })
   }

  }

}
