import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../employee.service';

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})
export class EmployeeListComponent implements OnInit {

  employees = []

  constructor(
    private employeeService: EmployeeService
  ) { }

  ngOnInit(): void {
  }

  loadEmployees(){
    this.employeeService.getEmployee()
    .subscribe(response => {
      if(response['status'] == 'success'){
        this.employees = response['data']
        console.log(response['data']);
        
      } else{
        console.log(response['error']);
        
      }
    })
  }

}
