import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { FormsModule } from '@angular/forms'
import { HttpClientModule } from '@angular/common/http';
import { LoginService} from './login.service';
import { EmployeeService} from './employee.service';

import { HomeComponent } from './home/home.component';
import { Home1Component } from './home1/home1.component';
import { EmployeeListComponent } from './employee-list/employee-list.component'


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    Home1Component,
    EmployeeListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
  ],
  providers: [
    LoginService,
    EmployeeService
    ],
  bootstrap: [AppComponent]
})
export class AppModule { }
