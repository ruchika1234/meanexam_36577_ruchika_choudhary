import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  url = 'http://localhost:3000/admin'

  constructor(
    private router: Router,
    private httpClient: HttpClient
  ) { }

  login(email: string, password: string){
    const body = {
      email: email,
      password: password
    }

    return this.httpClient.post(this.url + '/login', body)
  }
}
